# Spring template project #

To start configuring your project open the pom.xml and change the following:
	
```
#!xml

<groupId>template.project</groupId>
<artifactId>template-project</artifactId>
<version>0.0.1-SNAPSHOT</version>
<name>TODO: Template project</name>
<description>TODO: PUT HERE A DESCRIPTION.</description>
```

to correct values, then in web.xml change:
	
```
#!xml

<display-name>template-project</display-name>
```

in app-context.xml change:


```
#!xml

<context:component-scan base-package="template.project"> <!-- also rename the packages to the correct name -->
...
<jpa:repositories base-package="template.project.repository"
```

	
in database-dev.xml and database-prod.xml:

```
#!xml

<property name="packagesToScan" value="template.project.entity" />	
```


in dispatcher-servlet.xml change:

```
#!xml

<context:component-scan base-package="template.project.controller" />
```


in the goals of run option put:
	
```
#!java

-Dspring.profiles.active="dev" // for use the development database
-Dspring.profiles.active="prod"// for use the production database
```