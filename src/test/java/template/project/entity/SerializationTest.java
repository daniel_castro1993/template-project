/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package template.project.entity;

import java.sql.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import template.project.utils.ObjectFormConverter;

import static org.junit.Assert.*;

/**
 *
 * @author Daniel Castro
 */
public class SerializationTest {

  private ObjectFormConverter converter;

  private static class SimpleDto {

    String aString;
  }

  private static class MediumDto {

    int aInt;
    Integer aInteger;
    long aLong;
    float aFloat;
    double aDouble;
    Date aDate;
    SimpleDto simple;
  }

  private static class ComplexDto {

    SimpleDto simple;
    MediumDto medium;
    ComplexDto complex;
  }

  public SerializationTest() {
  }

  @BeforeClass
  public static void setUpClass() {
  }

  @AfterClass
  public static void tearDownClass() {
  }

  @Before
  public void setUp() {
    converter = ObjectFormConverter.getInstance();
  }

  @After
  public void tearDown() {
  }

  @Test
  public void complexSerialize() {
    SimpleDto simple = new SimpleDto();
    simple.aString = "6";

    MediumDto medium = new MediumDto();
    medium.aInt = 3;
    medium.aInteger = 4;
    medium.aLong = 5;
    medium.aFloat = 2f;
    medium.aDouble = 1.0;
    medium.aDate = Date.valueOf("2016-01-01");
    medium.simple = simple;
    medium.simple.aString = "6";

    ComplexDto complex1 = new ComplexDto();
    ComplexDto complex2 = new ComplexDto();

    complex1.simple = simple;
    complex1.medium = medium;
    complex1.complex = complex2;

    complex2.simple = simple;
    complex2.medium = medium;
    complex2.complex = complex1;

    String testStr = converter.serializeToJson(complex1);

    assertNotNull("wrong serialization", testStr);

    ComplexDto desDTO = converter.deserialize(testStr, complex1.getClass());

    assertNotNull(desDTO);
    assertEquals("wrong deserialization",
            "6", desDTO.complex.simple.aString);
    assertEquals("wrong deserialization",
            2f, desDTO.complex.complex.medium.aFloat, 0.01);
    assertEquals("wrong deserialization",
            3, desDTO.complex.complex.complex.medium.aInt);
    assertEquals("wrong deserialization",
            Date.valueOf("2016-01-01"),
            desDTO.complex.complex.complex.complex.medium.aDate);
  }

  @Test
  public void mediumSerialize() {
    MediumDto test = new MediumDto();
    test.aInt = 3;
    test.aInteger = 4;
    test.aLong = 5;
    test.aFloat = 2f;
    test.aDouble = 1.0;
    test.aDate = Date.valueOf("2016-01-01");
    test.simple = new SimpleDto();
    test.simple.aString = "6";

    String testStr = converter.serializeToJson(test);

    assertEquals("wrong serialization",
            "{\"aInt\":3,\"aLong\":5,\"aFloat\":2.0,\"aDate\":\"2016-01-01\","
            + "\"aDouble\":1.0,\"simple\":{\"aString\":\"6\"},\"aInteger\":4}",
            testStr);

    MediumDto deserializedDTO = converter.deserialize(testStr, test.getClass());

    assertNotNull(deserializedDTO);
    assertEquals("wrong deserialization",
            "6", deserializedDTO.simple.aString);
    assertEquals("wrong deserialization",
            2f, deserializedDTO.aFloat, 0.01);
    assertEquals("wrong deserialization",
            3, deserializedDTO.aInt);
    assertEquals("wrong deserialization",
            Date.valueOf("2016-01-01"), deserializedDTO.aDate);
  }

  @Test
  public void simpleSerialize() {
    SimpleDto test = new SimpleDto();
    test.aString = "Hello world!";

    String testStr = converter.serializeToJson(test);

    assertEquals("wrong serialization",
            "{\"aString\":\"Hello world!\"}", testStr);

    SimpleDto deserializedDTO = converter.deserialize(testStr, test.getClass());

    assertNotNull(deserializedDTO);
    assertEquals("wrong deserialization",
            "Hello world!", deserializedDTO.aString);
  }
}
