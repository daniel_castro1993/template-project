$(document).on("init.dt", function () {
  // uses bootstrap styling

  $("div.dataTables_length").addClass("form-inline form-group");
  $("div.dataTables_filter").addClass("form-inline form-group");

  $("div.dataTables_length select").addClass("form-control input-sm");
  $("div.dataTables_filter input").addClass("form-control input-sm");
});

