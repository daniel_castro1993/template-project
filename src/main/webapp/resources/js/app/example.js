
$(document).ready(function () {
  $("table#example-table").dataTable({
    "serverSide": true,
    "ajax": "/ajax/books/datatables",
    "columnDefs": [
      {"name": "name", "targets": 0},
      {"name": "gender", "targets": 1},
      {"name": "price", "targets": 2},
      {"name": "available", "targets": 3}
    ]
  });
});



