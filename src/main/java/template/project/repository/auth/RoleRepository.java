package template.project.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import template.project.entity.auth.Role;
import template.project.entity.auth.Role.Options;

public interface RoleRepository extends JpaRepository<Role, Integer> {

  Role findByRole(Options role);

}
