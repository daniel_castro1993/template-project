package template.project.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import template.project.entity.auth.User;

public interface UserRepository extends JpaRepository<User, Integer> {

  User findByUsername(String username);

  @Modifying
  @Transactional
  @Query(
          "UPDATE User u "
          + "SET u.lastAccess = CURRENT_TIMESTAMP "
          + "WHERE u.username = :username"
  )
  int setLastAccessNow(@Param("username") String username);
  
  @Modifying
  @Transactional
  @Query(
          "UPDATE User u "
          + "SET u.tries = u.tries + 1 "
          + "WHERE u.username = :username"
  )
  int incRetries(@Param("username") String username);
  
  @Modifying
  @Transactional
  @Query(
          "UPDATE User u "
          + "SET u.tries = 0 "
          + "WHERE u.username = :username"
  )
  int setRetriesZero(@Param("username") String username);

}
