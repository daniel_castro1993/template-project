/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package template.project.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.javatuples.Pair;

/**
 *
 */
public final class MetaObject {

  private final static Logger LOGGER = Logger.getLogger(MetaObject.class);

  public static Map<String, Field> getAllFields(Map<String, Field> fields,
          Class<?> type) {

    Map<String, Field> map = Arrays.asList(type.getDeclaredFields())
            .parallelStream().map(f -> Pair.with(f.getName(), f)
            ).collect(Collectors.toConcurrentMap(
                    pair -> pair.getValue0(),
                    pair -> pair.getValue1()));

    map.forEach((name, f) -> {
      fields.putIfAbsent(name, f);
    });

    if (type.getSuperclass() != Object.class) {
      getAllFields(fields, type.getSuperclass());
    }

    return fields;
  }

  public static Map<String, Method> getAllMethods(Map<String, Method> methods,
          Class<?> type) {

    Map<String, Method> map = Arrays.asList(type.getDeclaredMethods())
            .parallelStream().map(m -> Pair.with(m.getName(), m)
            ).collect(Collectors.toConcurrentMap(
                    pair -> pair.getValue0(),
                    pair -> pair.getValue1()));

    map.forEach((name, m) -> {
      methods.putIfAbsent(name, m);
    });

    if (type.getSuperclass() != Object.class) {
      getAllMethods(methods, type.getSuperclass());
    }

    return methods;
  }

  public static Method findMethod(Class<?> type, String name,
          Object... params) {

    Method method = Arrays.asList(type.getDeclaredMethods())
            .parallelStream().filter(m -> {
              Class[] types = m.getParameterTypes();

              if (types.length != params.length) {
                return false;
              }

              for (int i = 0; i < types.length; i++) {
                if (types[i] != params[i]) {
                  return false;
                }
              }

              return m.getName().equals(name);
            }).findAny().orElse(null);

    Class superClass = type.getSuperclass();
    if (method == null && (superClass != Object.class && superClass != null)) {
      return findMethod(superClass, name, params);
    }

    return method;
  }

  /**
   *
   * @param dest
   * @param source
   */
  public static void paramCopy(Object dest, Object source) {
    Class destT = dest.getClass();
    Class sourceT = source.getClass();

    Map<String, Field> destF = getAllFields(new HashMap<>(), destT);
    Map<String, Field> sourceF = getAllFields(new HashMap<>(), sourceT);

    destF.forEach((name, f) -> {
      String get = "get" + name.substring(0, 1).toUpperCase()
              + name.substring(1);
      String set = "set" + name.substring(0, 1).toUpperCase()
              + name.substring(1);

      Method destM = findMethod(destT, set, f.getType());
      Method sourceM = findMethod(sourceT, get);

      if (sourceM == null
              && (f.getType().isAssignableFrom(boolean.class)
              || f.getType().isAssignableFrom(Boolean.class))) {

        // if is a boolean try with is
        String is = "is" + name.substring(0, 1).toUpperCase()
                + name.substring(1);
        sourceM = findMethod(sourceT, is);
      }

      if (destM == null && f.getType().isAssignableFrom(Collection.class)) {

        // if is a collection try with addAll
        String addAll = "addAll" + name.substring(0, 1).toUpperCase()
                + name.substring(1);
        destM = findMethod(destT, addAll, f.getType());
      }

      if (sourceM == null || destM == null) {
        return;
      }

      if (sourceF.containsKey(name)) {
        try {

          destM.invoke(dest, sourceM.invoke(source));

        } catch (SecurityException | IllegalAccessException |
                IllegalArgumentException | InvocationTargetException ex) {
          // cannot copy this field
          LOGGER.log(Level.WARN, "Incompatible parameter \"" + name
                  + "\" in destiny class \"" + destT.getSimpleName()
                  + "\"; either the correct getters/setters are not "
                  + "defined (\"" + get + "\" / \"" + set
                  + "\") or the parameter is not "
                  + "present in source class \"" + sourceT.getSimpleName()
                  + "\"");
        }
      }
    });
  }
}
