package template.project.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.util.ClassUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 *
 */
public final class ObjectFormConverter {

  private final static int MAX_DEPTH = 10;

  private final static Logger LOGGER
          = Logger.getLogger(ObjectFormConverter.class);

  private final static ObjectFormConverter INSTANCE = new ObjectFormConverter();

  private final SmartValidator validator;

  private ObjectFormConverter() {
    validator = new LocalValidatorFactoryBean();
  }

  public static ObjectFormConverter getInstance() {
    return INSTANCE;
  }

  public String serializeToJson(Object obj) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.writeValueAsString(serialize(obj));
    } catch (JsonProcessingException ex) {
      LOGGER.log(Level.WARN, "Couldn't serialize into string", ex);
    }

    return null;
  }

  private Map<String, Object> serialize(Object obj, int depth) {
    Class c = obj.getClass();
    Map<String, Object> map = new HashMap<>();

    Field[] fields = c.getDeclaredFields();

    for (Field f : fields) {
      f.setAccessible(true);
      FormParameter param = f.getDeclaredAnnotation(FormParameter.class);

      try {
        Object val = f.get(obj);

        if (isPrimitive(val)) {
          map.put(f.getName(), val);
        } else if (hasGoodToString(val)) {
          map.put(f.getName(), val.toString());
        } else if (depth < MAX_DEPTH) {
          map.put(f.getName(), serialize(val, depth + 1));
        } else {
          map.put(f.getName(), val.toString());
        }
      } catch (IllegalArgumentException | IllegalAccessException ex) {
        LOGGER.log(Level.WARN, "Couldn't add values into map", ex);
      }

      if (param != null) {
        Map<String, Object> params = new HashMap<>();
        List<Object> attrs = new LinkedList<>();
        Class t = f.getType();

        map.put(f.getName() + ".formInfo", params);
        params.put("cntClass", param.cntClass());
        params.put("type", param.type());
        params.put("attrs", attrs);
        for (Attr a : param.cntExtraAttr()) {
          Map<String, String> attr = new HashMap<>();
          attr.put(a.name(), a.value());
          attrs.add(attrs);
        }

        if (t.isEnum()) {
          List<String> enumOptions = new LinkedList<>();
          for (Object o : t.getEnumConstants()) {
            enumOptions.add(o.toString());
          }
          params.put("options", enumOptions);
        }
      }
    }

    return map;
  }

  public Map<String, Object> serialize(Object obj) {
    return serialize(obj, 0);
  }

  public <T> List<T> deserializeList(String jsonForm, Class<T> c) {
    ObjectMapper mapper = new ObjectMapper();
    JavaType listT = mapper.getTypeFactory()
            .constructCollectionType(List.class, c);
    if (mapper.canDeserialize(listT)) {
      JavaType t = mapper.getTypeFactory()
              .constructCollectionType(List.class, c);

      try {
        return mapper.readValue(jsonForm, t);
      } catch (IOException ex) {
        LOGGER.log(Level.WARN, "Couldn't deserialize into list", ex);
      }
    }
    return null;
  }

  public <T> Map<String, T> deserializeMap(String jsonForm, Class<T> c) {
    ObjectMapper mapper = new ObjectMapper();
    JavaType mapT = mapper.getTypeFactory()
            .constructMapType(Map.class, String.class, c);

    if (mapper.canDeserialize(mapT)) {
      try {
        return mapper.readValue(jsonForm, mapT);
      } catch (IOException ex) {
        LOGGER.log(Level.WARN, "Couldn't deserialize into list", ex);
      }
    }
    return null;
  }

  public <T> T deserialize(String jsonForm, Class<T> c) {
    ObjectMapper mapper = new ObjectMapper();
    JavaType t = mapper.getTypeFactory()
            .constructType(c);

    if (mapper.canDeserialize(t)) {
      try {
        Constructor<T> constructor;
        constructor = c.getDeclaredConstructor();
        constructor.setAccessible(true);
        T res = constructor.newInstance();
        deserializeObj(jsonForm, res, 0);
        return res;
      } catch (IOException | InstantiationException |
              IllegalAccessException | NoSuchMethodException |
              SecurityException | IllegalArgumentException |
              InvocationTargetException ex) {
        LOGGER.log(Level.WARN,
                "Couldn't deserialize into the given class", ex);
      }
    }

    return null;
  }

  public <T> T deserializeValidate(String jsonForm,
          Class<T> c, Errors errors) {

    T res = deserialize(jsonForm, c);

    validator.validate(res, errors);

    return res;
  }

  private Object deserializeClass(String jsonStr, Class c)
          throws IOException {

    ObjectMapper mapper = new ObjectMapper();
    mapper.configure(
            DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    JavaType t = mapper.getTypeFactory().constructType(c);
    return mapper.readValue(jsonStr, t);
  }

  private void deserializeObj(String jsonStr, Object objToFill, int depth)
          throws IOException {

    Class c = objToFill.getClass();
    ObjectMapper mapper = new ObjectMapper();
    List<Map<String, Object>> data;

    try {
      data = mapper.readValue(jsonStr, List.class);
    } catch (IOException ex) {
      data = new ArrayList<>();
      data.add(mapper.readValue(jsonStr, Map.class));
    }

    data.stream().forEach((m) -> {
      m.forEach((key, val) -> {
        try {
          String value = mapper.writeValueAsString(val);
          Field f = c.getDeclaredField(key);

          f.setAccessible(true);

          Class fieldT = f.getType();

          if (isPrimitive(fieldT)) {
            f.set(objToFill, primitiveToObject(val, fieldT));
          } else {

            try {
              Constructor<?> constructor;
              constructor = fieldT.getDeclaredConstructor();
              constructor.setAccessible(true);
              Object res = constructor.newInstance();

              if (depth < MAX_DEPTH) {
                deserializeObj(value, res, depth + 1);
              }

              f.set(objToFill, res);

            } catch (IllegalArgumentException | InvocationTargetException |
                    NoSuchMethodException | InstantiationException ex2) {

              JavaType t = mapper.getTypeFactory().constructType(fieldT);
              f.set(objToFill, mapper.readValue(value, t));

            }
          }
        } catch (NoSuchFieldException | SecurityException |
                IllegalArgumentException | IllegalAccessException |
                IOException ex) {

          LOGGER.log(Level.WARN, "error deserializing", ex);
        }
      });
    });
  }

  private Object primitiveToObject(Object primitive, Class target) {

    String primitiveStr = primitive.toString();

    if (target == byte.class) {
      return new Byte(primitiveStr);
    } else if (target == short.class) {
      return new Short(primitiveStr);
    } else if (target == int.class) {
      return new Integer(primitiveStr);
    } else if (target == long.class) {
      return new Long(primitiveStr);
    } else if (target == float.class) {
      return new Float(primitiveStr);
    } else if (target == double.class) {
      return new Double(primitiveStr);
    } else if (target == boolean.class) {
      return new Boolean(primitiveStr);
    } else if (target == char.class) {
      return new Character((char) primitive);
    }

    return primitive;
  }

  private boolean isPrimitive(Object val) {
    return isPrimitive(val.getClass());
  }

  private boolean isPrimitive(Class t) {
    return ClassUtils.isAssignable(Byte.class, t)
            || ClassUtils.isAssignable(Short.class, t)
            || ClassUtils.isAssignable(Integer.class, t)
            || ClassUtils.isAssignable(Long.class, t)
            || ClassUtils.isAssignable(Float.class, t)
            || ClassUtils.isAssignable(Double.class, t)
            || ClassUtils.isAssignable(Boolean.class, t)
            || ClassUtils.isAssignable(Character.class, t)
            || ClassUtils.isAssignable(String.class, t);
  }

  private boolean hasGoodToString(Object val) {
    return hasGoodToString(val.getClass());
  }

  private boolean hasGoodToString(Class t) {
    return ClassUtils.isAssignable(Date.class, t)
            || ClassUtils.isAssignable(Time.class, t)
            || ClassUtils.isAssignable(Timestamp.class, t);
  }

}
