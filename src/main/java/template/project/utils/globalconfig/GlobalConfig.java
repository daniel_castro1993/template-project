package template.project.utils.globalconfig;

public class GlobalConfig {

  private static final GlobalConfig INSTANCE = new GlobalConfig();

  private String profile;

  public static GlobalConfig getInstance() {
    return INSTANCE;
  }

  private GlobalConfig() {
  }

  /**
   * Gets the value of profile.
   *
   * @return The value of profile.
   */
  public String getProfile() {
    return profile;
  }

  /**
   * Sets the value of profile.
   *
   * @param profile The value to set.
   */
  public void setProfile(String profile) {
    this.profile = profile;
  }
}
