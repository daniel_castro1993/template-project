package template.project.utils;

/**
 *
 */
public @interface Attr {
  String name() default "";
  String value() default "";
}
