package template.project.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FormParameter {
  String cntClass() default "";
  String inputClass() default "";
  String labelClass() default "";
  String inputCntClass() default "";
  String labelCntClass() default "";
  String type() default "text";
  String pattern() default "";
  boolean required() default false;
  Attr[] cntExtraAttr() default {};
}
