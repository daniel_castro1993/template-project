package template.project.service;

import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import template.project.utils.globalconfig.GlobalConfig;

public class ProfileConfig {

  @Autowired
  private Environment env;

  private final Logger logger = Logger.getLogger(ProfileConfig.class);

  public void init() {
    GlobalConfig config = GlobalConfig.getInstance();

    List<String> profiles = Arrays.asList(env.getActiveProfiles());
    String profile;

    if (profiles.contains("stage")) {
      profile = "stage";
    } else if (profiles.contains("prod")) {
      profile = "prod";
    } else {
      profile = "dev";
    }
    config.setProfile(profile);
  }

}
