package template.project.service;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import template.project.dto.UserDTO;
import template.project.entity.auth.User;
import template.project.repository.auth.UserRepository;
import template.project.utils.MetaObject;

@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;
  
  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  private UserDTO fromEntity(User user) {
    UserDTO userDTO = new UserDTO();
    MetaObject.paramCopy(userDTO, user);
    return userDTO;
  }

  private User userfromDTO(UserDTO userDTO) {
    User user = new User();
    MetaObject.paramCopy(user, userDTO);
    return user;
  }

  public List<UserDTO> getAllUsers() {
    List<UserDTO> usersDTO;
    List<User> users = userRepository.findAll();
    usersDTO = users.stream().map((user) -> {
      int id = user.getId();
      return findUserWithId(id);
    }).collect(Collectors.toList());

    return usersDTO;
  }

  public UserDTO findUserWithId(int id) {
    User user = userRepository.getOne(id);
    UserDTO res = fromEntity(user);

    return res;
  }

  public UserDTO findUserWithUsername(String username) {
    User user = userRepository.findByUsername(username);
    
    return fromEntity(user);
  }

  public long countNumberOfUsers() {
    return userRepository.count();
  }

  public void addUser(UserDTO userDTO) {
    User user = userfromDTO(userDTO);
    
    user.setPassword(passwordEncoder.encode(user.getPassword()));

    userRepository.saveAndFlush(user);
  }

  public UserDTO updateLastAccess(UserDTO userDTO) {
    User user = userfromDTO(userDTO);

    userRepository.setLastAccessNow(user.getUsername());

    return findUserWithUsername(user.getUsername());
  }

}
