package template.project.service;

import javax.persistence.Transient;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import template.project.entity.auth.User;
import template.project.repository.auth.UserRepository;

/**
 *
 */
@Service
public class AuthService implements UserDetailsService {

  private final Logger logger = Logger.getLogger(AuthService.class);

  @Autowired
  private UserRepository userRepository;

  @Override
  public UserDetails loadUserByUsername(String username) throws
          UsernameNotFoundException {

    User user = userRepository.findByUsername(username);

    if (user == null) {
      logger.log(Level.INFO, "Username not found");
      throw new UsernameNotFoundException("Username not found");
    }

    return user;
  }

  public void setLastAccessNow(String username) {
    logger.log(Level.INFO, "Setting last access to now");
    userRepository.setLastAccessNow(username);
  }

}
