/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package template.project.service;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import template.project.entity.auth.Role;
import template.project.entity.auth.Role.Options;
import template.project.entity.auth.User;
import template.project.repository.auth.RoleRepository;
import template.project.repository.auth.UserRepository;

/**
 *
 */
public class PopulateService {

  private final Logger logger = Logger.getLogger(PopulateService.class);

  @Autowired
  private UserRepository userRep;

  @Autowired
  private RoleRepository roleRep;

  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  public void init() {
    Role role;
    User user;
    String pass;

    logger.log(Level.INFO, "Populate database");

    role = roleRep.findByRole(Options.USER);
    if (role == null) {
      role = new Role("USER");
      roleRep.save(role);
    }
    role = roleRep.findByRole(Options.ADMIN);
    if (role == null) {
      role = new Role("ADMIN");
      roleRep.save(role);
    }

    roleRep.flush();

    user = userRep.findByUsername("user");
    if (user == null) {
      pass = passwordEncoder.encode("user");
      user = new User("user", pass, new String[]{"USER"});
      userRep.save(user);
    }

    user = userRep.findByUsername("admin");
    if (user == null) {
      pass = passwordEncoder.encode("admin");
      user = new User("admin", pass, new String[]{"USER", "ADMIN"});
      userRep.save(user);
    }

    userRep.flush();
  }

}
