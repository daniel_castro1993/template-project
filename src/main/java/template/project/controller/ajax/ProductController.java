package template.project.controller.ajax;

import java.io.IOException;
import java.util.Map;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import template.project.dto.ProductDTO;
import template.project.utils.ObjectFormConverter;

/**
 *
 */
public class ProductController {

  private final ObjectFormConverter converted
          = ObjectFormConverter.getInstance();

  @RequestMapping(value = "/ajax/product/meta", produces = "application/json")
  @ResponseBody
  public Map<String, Object> getProductMeta() {
    return converted.serialize(ProductDTO.class);
  }

  @RequestMapping(value = "/ajax/product/add", produces = "application/json")
  @ResponseBody
  public void addProduct(@RequestParam("name") String name,
          @RequestParam("price") double price,
          @RequestParam("description") String description,
          @RequestParam("file") MultipartFile file) throws IOException {

    // TODO: check file
    ProductDTO productDTO = new ProductDTO(name, price, description);
    productDTO.setImage(file.getBytes());

    // TODO: service to add the product
  }
}
