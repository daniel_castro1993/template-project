package template.project.controller.ajax;

import com.fasterxml.jackson.annotation.JsonView;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.javatuples.Unit;
import template.project.utils.datatables.DataTablesInput;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import template.project.dto.BookDTO;
import template.project.dto.BookDTO.Gender;
import template.project.dto.TestDTO;
import template.project.utils.ObjectFormConverter;
import template.project.utils.datatables.ColumnParameter;
import template.project.utils.datatables.DataTablesOutput;
import template.project.utils.datatables.OrderParameter;

@Controller
public class AJAXController {

  private final List<BookDTO> booksMock;

  private final ObjectFormConverter converted
          = ObjectFormConverter.getInstance();

  {
    booksMock = new ArrayList<>(1000);
    Random rand = new Random();

    for (int i = 0; i < 1000; i++) {
      String name = "book-" + i;
      double price = i * rand.nextFloat();
      Gender gender = Gender.values()[rand.nextInt(3)];

      booksMock.add(new BookDTO(name, gender, price, true));

    }
  }

  @RequestMapping(
          value = "/ajax/helloworld",
          produces = "application/json",
          method = RequestMethod.POST)
  public @ResponseBody
  Unit<String> getHelloWorld() {

    return Unit.with("Hello world!");
  }

  @RequestMapping(
          value = "/ajax/book/form",
          produces = "application/json",
          method = RequestMethod.POST)
  public @ResponseBody
  Map<String, Object> getBookMeta() {

    return converted.serialize(BookDTO.class);
  }

  @RequestMapping(
          value = "/ajax/book/set",
          produces = "application/json",
          method = RequestMethod.POST)
  public @ResponseBody
  void setBook(@RequestBody String book, BindingResult errors) {

    BookDTO bookInstance = ObjectFormConverter.getInstance()
            .deserializeValidate(book, BookDTO.class, errors);
    
    System.out.println("book name: " + bookInstance.getName());
    System.out.println("book price: " + bookInstance.getPrice());
    System.out.println("book isAvailable: " + bookInstance.isAvailable());
    System.out.println("book gender:" + bookInstance.getGender());
  }

  @JsonView(DataTablesOutput.View.class)
  @RequestMapping(
          value = "/ajax/test/stuff",
          method = RequestMethod.POST,
          consumes = "application/json")
  public void getUsers(@RequestBody String test) {

    TestDTO testDTO = converted.deserialize(test, TestDTO.class);

    System.out.println("request body: " + test);
  }

  @JsonView(DataTablesOutput.View.class)
  @RequestMapping("/ajax/books/datatables")
  public DataTablesOutput<BookDTO> getUsersDatatables(
          @RequestBody @ModelAttribute("input") String inputStr,
          BindingResult errors) {

    DataTablesInput input = ObjectFormConverter.getInstance()
            .deserializeValidate(inputStr, DataTablesInput.class, errors);

    
    final int draw = input.getDraw();
    final int start = input.getStart();
    final int length = input.getLength();
    final String search = input.getSearch().getValue();
    final boolean isRegex = input.getSearch().getRegex();
    final List<OrderParameter> orders = input.getOrder();
    final List<ColumnParameter> columns = input.getColumns();

    List<BookDTO> data = booksMock;

    if (orders.isEmpty()) {
      // no order
      data = booksMock.subList(start, length);
    } else {
      // order

      booksMock.sort((b1, b2) -> {
        for (int i = 0; i < orders.size(); i++) {
          OrderParameter o = orders.get(i);
          String column = columns.get(o.getColumn()).getName();
          String dir = o.getDir();

          int cmp = 0;

          switch (column) {
            case "name":
              cmp = b1.getName().compareTo(b2.getName());
              break;
            case "gender":
              cmp = b1.getGender().toString().compareTo(
                      b1.getGender().toString());
              break;
            case "price":
              cmp = new Double(b1.getPrice()).compareTo(b2.getPrice());
              break;
            case "available":
              cmp = new Boolean(b1.isAvailable()).compareTo(b2.isAvailable());
              break;
            default:
              break;
          }
          if (cmp != 0) {
            if (dir.equals("desc")) {
              cmp *= -1;
            }
            return cmp;
          }
        }

        return 0;
      });

    }

    if (search == null || search.isEmpty()) {
      // no search
    } else {
      // search

    }

    DataTablesOutput<BookDTO> res = new DataTablesOutput<>();

    res.setData(data);
    res.setDraw(draw);
    res.setRecordsTotal(new Long(booksMock.size()));
    res.setRecordsFiltered(new Long(data.size()));

    return res;
  }

}
