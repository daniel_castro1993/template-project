package template.project.controller.pages;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import template.project.dto.BookDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import template.project.dto.BookDTO.Gender;

@Controller
public class IndexController {

  @RequestMapping({"/", "/index"})
  public String index() {
    return "index";
  }

  @RequestMapping("/login")
  public String login() {
    return "login";
  }

  @RequestMapping("/logout")
  public String logoutPage(HttpServletRequest request,
          HttpServletResponse response) {

    Authentication auth = SecurityContextHolder
            .getContext().getAuthentication();

    if (auth != null) {
      new SecurityContextLogoutHandler().logout(request, response, auth);
    }
    return "redirect:/login?logout";
  }

  @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Username not found")
  @ExceptionHandler(UsernameNotFoundException.class)
  public void usernameNotFound() {

  }

  @RequestMapping("/books")
  public String example(Model model) {
    List<BookDTO> books = new ArrayList<>();
    books.add(new BookDTO("The Hitchhiker's Guide to the Galaxy",
            Gender.COMEDY, 5.70, true));
    books.add(new BookDTO("Life, the Universe and Everything",
            Gender.COMEDY, 5.60, false));
    books.add(new BookDTO("The Restaurant at the End of the Universe",
            Gender.COMEDY, 5.40, true));

    model.addAttribute("books", books);

    return "books";
  }
  
  @RequestMapping("/angular-example")
  public String angularExample(Model model) {
    return "angular-example";
  }

}
