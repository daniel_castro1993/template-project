package template.project.controller.pages;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@Controller
public class ErrorController {

  @RequestMapping("/404")
  public String notFound() {
    return "errors/not-found";
  }

  @RequestMapping("/403")
  public String forbiden() {
    return "errors/access-denied";
  }

}
