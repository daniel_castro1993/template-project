package template.project.controller.pages;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

  @RequestMapping("/user")
  public String adminView(Model model, HttpSession session) {

    model.addAttribute("user.name", session.getAttribute("user.name"));

    return "internal-page";
  }
}
