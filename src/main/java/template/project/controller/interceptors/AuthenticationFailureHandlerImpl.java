/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package template.project.controller.interceptors;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import template.project.service.AuthService;

/**
 *
 */
public class AuthenticationFailureHandlerImpl
        implements AuthenticationFailureHandler {

  @Autowired
  private AuthService authService;
  
  @Override
  public void onAuthenticationFailure(HttpServletRequest req,
          HttpServletResponse res, AuthenticationException a) throws
          IOException, ServletException {
    
    res.sendRedirect("/login?login=failed");
  }

}
