package template.project.controller.interceptors;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import template.project.utils.globalconfig.GlobalConfig;

@ControllerAdvice
public class ModelAttrsAdder {

  @Autowired
  private Environment env;

  private String profile = null;

  @ModelAttribute
  public void globalAttributes(HttpServletRequest req,
          HttpServletResponse res, Model model, HttpSession session) {
    if (session.getAttribute("auth") != null) {
      model.addAttribute("auth",
              session.getAttribute("auth"));
      model.addAttribute("auth.user.username",
              session.getAttribute("auth.user.username"));
    }

    CsrfToken csrfToken
            = (CsrfToken) req.getAttribute(CsrfToken.class.getName());
    if (csrfToken != null) {
      model.addAttribute("_csrf", csrfToken);
    }

  }

  @ModelAttribute
  public void globalAttributesDev(Model model) {
    GlobalConfig config = GlobalConfig.getInstance();

    model.addAttribute("__global_config", config);
  }

}
