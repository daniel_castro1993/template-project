package template.project.controller.interceptors;

import java.io.IOException;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import template.project.service.AuthService;

/**
 *
 */
public class AuthenticationSuccessHandlerImpl
        implements AuthenticationSuccessHandler {

  private final Logger logger
          = Logger.getLogger(AuthenticationSuccessHandlerImpl.class);

  @Autowired
  private AuthService authService;

  @Override
  public void onAuthenticationSuccess(HttpServletRequest req,
          HttpServletResponse res, Authentication a) throws IOException,
          ServletException {

    HttpSession session = req.getSession();

    session.setAttribute("auth", true);
    session.setAttribute("auth.user.username", a.getName());

    authService.setLastAccessNow(a.getName());

    String page = req.getParameter("_goToPage");
    Object url_prior_login = session.getAttribute("url_prior_login");

    if (page != null) {
      res.sendRedirect(page);
    } else {
      res.sendRedirect(determineTargetUrl(a));
    }
  }

  protected String determineTargetUrl(Authentication auth) {
    Set<String> authorities
            = AuthorityUtils.authorityListToSet(auth.getAuthorities());
    if (authorities.contains("ADMIN")) {
      return "/admin";
    } else if (authorities.contains("USER")) {
      return "/user";
    } else {
      throw new IllegalStateException();
    }
  }

}
