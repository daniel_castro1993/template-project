package template.project.controller.interceptors;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

/**
 *
 */
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {

  @Override
  public void onLogoutSuccess(HttpServletRequest req, HttpServletResponse res,
          Authentication a) throws IOException, ServletException {

    // session is invalidated by the default configuration
    res.sendRedirect("/");
  }

}
