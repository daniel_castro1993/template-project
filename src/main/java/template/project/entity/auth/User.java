package template.project.entity.auth;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(
        name = "auth_user",
        uniqueConstraints = @UniqueConstraint(columnNames = {"username"})
)
public class User implements Serializable, UserDetails {

  public static final long TIME_TO_RENEW_PASSWORD = 3600000; // in milliseconds
  public static final int MAX_TRIES = 3;

  @Transient
  private final Logger logger = Logger.getLogger(User.class);
  
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "user_id")
  private int id;

  @Column(name = "username")
  private String username;

  @Column(name = "password")
  private String password;

  @Column(name = "tries")
  private int tries = 0;

  @Column(name = "active")
  private boolean active = true;

  @Column(name = "expired")
  private boolean expired = false;

  @Column(name = "locked")
  private boolean locked = false;

  @Column(name = "last_access")
  private Timestamp lastAccess;

  @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinTable(name = "user_roles")
  private Set<Role> roles;

  public User() {
  }

  public User(String name) {
    this.username = name;
  }

  public User(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public User(String username, String password, String[] roles) {
    this.username = username;
    this.password = password;

    this.roles = Arrays.asList(roles).stream()
            .map((role) -> new Role(role))
            .collect(Collectors.toSet());
  }

  /**
   * Gets the value of lastAccess.
   *
   * @return The value of lastAccess.
   */
  public Timestamp getLastAccess() {
    return lastAccess;
  }

  /**
   * Sets the value of lastAccess.
   *
   * @param lastAccess The value to set.
   */
  public void setLastAccess(Timestamp lastAccess) {
    this.lastAccess = lastAccess;
  }

  public void setLastAccessNow() {
    this.lastAccess = new Timestamp(System.currentTimeMillis());
  }

  /**
   * Gets the value of locked.
   *
   * @return The value of locked.
   */
  public boolean getLocked() {
    return locked;
  }

  /**
   * Sets the value of locked.
   *
   * @param locked The value to set.
   */
  public void setLocked(boolean locked) {
    this.locked = locked;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    logger.log(Priority.INFO, username + "> authorities: " + roles);
    return new LinkedList<>(roles);
  }

  @Override
  public boolean isAccountNonExpired() {
    logger.log(Priority.INFO, username + "> expired: " + expired);
    return !expired;
  }

  @Override
  public boolean isAccountNonLocked() {
    logger.log(Priority.INFO, username + "> locked: " + locked);
    return !locked;
  }

  @Override
  public boolean isCredentialsNonExpired() {

    if (lastAccess == null) {
      logger.log(Priority.INFO, username + "> credentials non expired: true");
      return true;
    }

    long lastMillis = lastAccess.toInstant().toEpochMilli();
    Timestamp expire = new Timestamp(lastMillis + TIME_TO_RENEW_PASSWORD);

    logger.log(Priority.INFO, username + "> non expired: "
            + lastAccess.before(expire));

    return lastAccess.before(expire);
  }

  @Override
  public boolean isEnabled() {
    logger.log(Priority.INFO, username + "> enabled: " + active);
    return active;
  }

  /**
   * Gets the value of expired.
   *
   * @return The value of expired.
   */
  public boolean isExpired() {
    return expired;
  }

  /**
   * Sets the value of expired.
   *
   * @param expired The value to set.
   */
  public void setExpired(boolean expired) {
    this.expired = expired;
  }

  /**
   * Gets the value of password.
   *
   * @return The value of password.
   */
  @Override
  public String getPassword() {
    logger.log(Priority.INFO, username + "> password: " + password);
    return password;
  }

  /**
   * Sets the value of password.
   *
   * @param password The value to set.
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * Gets the value of active.
   *
   * @return The value of active.
   */
  public boolean isActive() {
    return active;
  }

  /**
   * Sets the value of active.
   *
   * @param active The value to set.
   */
  public void setActive(boolean active) {
    this.active = active;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }

  public void setRoles(String[] roles) {
    this.roles = Arrays.asList(roles).stream()
            .map((role) -> new Role(role))
            .collect(Collectors.toSet());
  }

  /**
   * Get the value of tries
   *
   * @return the value of tries
   */
  public int getTries() {
    return tries;
  }

  /**
   * Set the value of tries
   *
   * @param tries new value of tries
   */
  public void setTries(int tries) {
    this.tries = tries;
  }

  /**
   * Get the value of id
   *
   * @return the value of id
   */
  public int getId() {
    return id;
  }

  /**
   * Set the value of id
   *
   * @param id new value of id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Get the value of username
   *
   * @return the value of username
   */
  @Override
  public String getUsername() {
    return username;
  }

  /**
   * Set the value of username
   *
   * @param username new value of username
   */
  public void setUsername(String username) {
    this.username = username;
  }

}
