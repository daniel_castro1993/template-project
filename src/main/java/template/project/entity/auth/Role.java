package template.project.entity.auth;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "auth_role")
public class Role implements Serializable, GrantedAuthority {

  public static enum Options {
    USER, ADMIN
  }

  @Transient
  private final Logger logger = Logger.getLogger(Role.class);

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "role_id")
  private Long id;

  @ManyToMany(mappedBy = "roles", cascade = CascadeType.ALL)
  private Set<User> users;

  @Enumerated(EnumType.STRING)
  @Column(name = "role_name")
  private Options role;

  @Override
  public String getAuthority() {
    return role.toString();
  }

  public Role() {
  }

  public Role(Options role) {
    this.role = role;
  }

  public Role(String role) {
    this.role = Options.valueOf(role);
  }

  /**
   * Gets the value of role.
   *
   * @return The value of role.
   */
  public Options getRole() {
    return role;
  }

  /**
   * Sets the value of role.
   *
   * @param role The value to set.
   */
  public void setRole(Options role) {
    this.role = role;
  }

  /**
   * Gets the value of users.
   *
   * @return The value of users.
   */
  public Set<User> getUsers() {
    return users;
  }

  /**
   * Sets the value of users.
   *
   * @param users The value to set.
   */
  public void setUsers(Set<User> users) {
    this.users = users;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String toString() {
    return role.toString();
  }

}
