package template.project.entity.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import template.project.entity.util.Image;

@Entity
@Table(name = "sale_product")
public class Product implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "product_id")
  private int id;

  @OneToOne(fetch = FetchType.LAZY)
  private Image image;

  @Column(name = "product_name")
  private String name;

  @Column(name = "product_price")
  private double price;

  @Column(name = "description")
  private String description;

  public Product() {
  }

  public Product(int id) {
    this.id = id;
  }

  public Product(String name, double price, String description) {
    this.name = name;
    this.price = price;
    this.description = description;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Image getImage() {
    return image;
  }

  public void setImage(Image image) {
    this.image = image;
  }

}
