package template.project.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import template.project.utils.FormParameter;

/**
 *
 */
public class BookDTO {

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  public static enum Gender {
    ROMANCE, DRAMA, COMEDY
  }

  @NotNull
  @Length(max = 255, min = 3)
  @FormParameter
  private String name;

  @NotNull
  @Min(0)
  @FormParameter(type = "number")
  private double price;

  @NotNull
  @FormParameter(type = "checkbox")
  private boolean available;

  @NotNull
  @FormParameter(type = "radio")
  private Gender gender;

  public BookDTO() {
  }

  public BookDTO(String name, Gender gender, double price, boolean available) {
    this.name = name;
    this.gender = gender;
    this.price = price;
    this.available = available;
  }

  /**
   * Gets the value of gender.
   *
   * @return The value of gender.
   */
  public Gender getGender() {
    return gender;
  }

  /**
   * Sets the value of gender.
   *
   * @param gender The value to set.
   */
  public void setGender(Gender gender) {
    this.gender = gender;
  }

  /**
   * Gets the value of available.
   *
   * @return The value of available.
   */
  public boolean isAvailable() {
    return available;
  }

  /**
   * Sets the value of available.
   *
   * @param available The value to set.
   */
  public void setAvailable(boolean available) {
    this.available = available;
  }

  /**
   * Gets the value of price.
   *
   * @return The value of price.
   */
  public double getPrice() {
    return price;
  }

  /**
   * Sets the value of price.
   *
   * @param price The value to set.
   */
  public void setPrice(double price) {
    this.price = price;
  }

  /**
   * Gets the value of name.
   *
   * @return The value of name.
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of name.
   *
   * @param name The value to set.
   */
  public void setName(String name) {
    this.name = name;
  }

}
