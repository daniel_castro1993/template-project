package template.project.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import template.project.utils.FormParameter;

public class UserDTO implements Serializable {

  private int id;

  @FormParameter
  private String username;

  private Timestamp lastAccess;

  private boolean locked;

  private boolean active;

  private int tries;

  public UserDTO() {
  }

  public UserDTO(int id, String name) {
    this.id = id;
    this.username = name;
  }

  /**
   * Gets the value of tries.
   *
   * @return The value of tries.
   */
  public int getTries() {
    return tries;
  }

  /**
   * Sets the value of tries.
   *
   * @param tries The value to set.
   */
  public void setTries(int tries) {
    this.tries = tries;
  }

  /**
   * Gets the value of active.
   *
   * @return The value of active.
   */
  public boolean getActive() {
    return active;
  }

  /**
   * Sets the value of active.
   *
   * @param active The value to set.
   */
  public void setActive(boolean active) {
    this.active = active;
  }

  /**
   * Gets the value of locked.
   *
   * @return The value of locked.
   */
  public boolean isLocked() {
    return locked;
  }

  /**
   * Sets the value of locked.
   *
   * @param locked The value to set.
   */
  public void setLocked(boolean locked) {
    this.locked = locked;
  }

  /**
   * Gets the value of lastAccess.
   *
   * @return The value of lastAccess.
   */
  public Timestamp getLastAccess() {
    return lastAccess;
  }

  /**
   * Sets the value of lastAccess.
   *
   * @param lastAccess The value to set.
   */
  public void setLastAccess(Timestamp lastAccess) {
    this.lastAccess = lastAccess;
  }

  /**
   * Get the value of id
   *
   * @return the value of id
   */
  public int getId() {
    return id;
  }

  /**
   * Set the value of id
   *
   * @param id new value of id
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Get the value of username
   *
   * @return the value of username
   */
  public String getUsername() {
    return username;
  }

  /**
   * Set the value of username
   *
   * @param username new value of username
   */
  public void setUsername(String username) {
    this.username = username;
  }

}
