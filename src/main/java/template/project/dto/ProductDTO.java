package template.project.dto;

import template.project.utils.FormParameter;

/**
 *
 */
public class ProductDTO {

  private int id;

  @FormParameter
  private String name;

  @FormParameter(type = "number")
  private double price;

  @FormParameter
  private String description;

  @FormParameter(type = "file")
  private byte[] image;

  public ProductDTO() {
  }

  public ProductDTO(int id) {
    this.id = id;
  }

  public ProductDTO(String name, double price, String description) {
    this.name = name;
    this.price = price;
    this.description = description;
  }

  /**
   * Gets the value of id.
   *
   * @return The value of id.
   */
  public int getId() {
    return id;
  }

  /**
   * Sets the value of id.
   *
   * @param id The value to set.
   */
  public void setId(int id) {
    this.id = id;
  }

  /**
   * Gets the value of description.
   *
   * @return The value of description.
   */
  public String getDescription() {
    return description;
  }

  /**
   * Sets the value of description.
   *
   * @param description The value to set.
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * Gets the value of price.
   *
   * @return The value of price.
   */
  public double getPrice() {
    return price;
  }

  /**
   * Sets the value of price.
   *
   * @param price The value to set.
   */
  public void setPrice(double price) {
    this.price = price;
  }

  /**
   * Gets the value of image.
   *
   * @return The value of image.
   */
  public byte[] getImage() {
    return image;
  }

  /**
   * Sets the value of image.
   *
   * @param image The value to set.
   */
  public void setImage(byte[] image) {
    this.image = image;
  }

  /**
   * Gets the value of name.
   *
   * @return The value of name.
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the value of name.
   *
   * @param name The value to set.
   */
  public void setName(String name) {
    this.name = name;
  }
}
