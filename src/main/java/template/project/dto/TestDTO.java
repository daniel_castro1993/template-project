package template.project.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestDTO implements Serializable {

  @NotNull
  @Length(min = 3, max = 256)
  private String aString;

  public TestDTO() {
  }

  public TestDTO(String aString) {
    this.aString = aString;
  }

  /**
   * Gets the value of aString.
   *
   * @return The value of aString.
   */
  public String getAString() {
    return aString;
  }

  /**
   * Sets the value of aString.
   *
   * @param aString The value to set.
   */
  public void setAString(String aString) {
    this.aString = aString;
  }
}
